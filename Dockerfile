FROM ubuntu:20.04

RUN apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get -y install build-essential make autoconf automake libtool nasm make pkg-config git nodejs npm ruby ruby-dev libpng-dev imagemagick graphicsmagick pngquant && ( which ssh-agent ||  apt-get -y install openssh-client ) && apt-get clean

RUN gem update --system && gem install compass

RUN npm -g install bower && npm -g install gulp@^3.9.1
